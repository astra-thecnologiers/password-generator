from typing import Union

from pydantic import BaseModel


class PasswordModel(BaseModel):
    length: Union[int, None] = None
    digits: Union[bool, None] = False
    lowercase: Union[bool, None] = False
    uppercase: Union[bool, None] = False
    symbols: Union[bool, None] = False
