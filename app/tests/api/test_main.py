from fastapi.testclient import TestClient


def test_main(client: TestClient):
    response = client.get("/")
    assert response.status_code == 200
    expected = """
        <html>
            <head>
                <title>Password Generator</title>
            </head>
            <body>
                <h1>Welcome to Password Generator</h1>
                <h2>If you need to know some help go to <a href="/docs">API Documentation</a></h2>
            </body>
        </html>
        """
    assert response.text == expected
