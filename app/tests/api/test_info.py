from fastapi.testclient import TestClient

from app.main import settings


def test_info(client: TestClient):
    response = client.get(f"{settings.API_STR}/info")
    assert response.status_code == 200
    assert response.json() == {
        "app_name": settings.app_name,
        "app_description": settings.app_description,
        "pw_length": settings.pw_length,
        "flags": {
            "use_lowercase": settings.lowercase,
            "use_uppercase": settings.uppercase,
            "use_digits": settings.digits,
            "use_symbols": settings.symbols
        }
    }
