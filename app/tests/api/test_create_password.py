import string
import re

from fastapi.testclient import TestClient

from app.main import settings
from app.tests.utils.random_data import generate_random_data, check_data


def test_create_password(client: TestClient):
    data = generate_random_data()
    response = client.post(f"{settings.API_STR}/create_password/", json=data)
    status_code, detail = check_data(data)
    assert response.status_code == status_code
    if status_code != 200:
        assert response.json()["detail"] == detail
    else:
        assert data["length"] == len(response.json())
        if not data["lowercase"]:
            assert not bool(re.search(r'[a-z]', response.json()))
        if not data["uppercase"]:
            assert not bool(re.search(r'[A-Z]', response.json()))
        if not data["digits"]:
            assert not bool(re.search(r'\d', response.json()))
        if not data["symbols"]:
            assert not bool(re.search(r'[%s]' % string.punctuation, response.json()))


def test_create_password_only_length(client: TestClient):
    data = {
        "length": 20
    }
    response = client.post(f"{settings.API_STR}/create_password/", json=data)
    status_code, detail = check_data(data)
    assert response.status_code == status_code
    if response.status_code != status_code:
        assert response.json()["detail"] == detail


def test_create_password_only_lowercase(client: TestClient):
    data = {
        "length": 10,
        "lowercase": True
    }
    response = client.post(f"{settings.API_STR}/create_password/", json=data)
    status_code, detail = check_data(data)
    assert response.status_code == status_code
    if response.status_code != status_code:
        assert response.json()["detail"] == detail
    else:
        assert not bool(re.search(r'[%s]' % string.punctuation, response.json()))
        assert not bool(re.search(r'\d', response.json()))
        assert not bool(re.search(r'[A-Z]', response.json()))


def test_create_password_only_uppercase(client: TestClient):
    data = {
        "length": 10,
        "uppercase": True
    }
    response = client.post(f"{settings.API_STR}/create_password/", json=data)
    status_code, detail = check_data(data)
    assert response.status_code == status_code
    if response.status_code != status_code:
        assert response.json()["detail"] == detail
    else:
        assert not bool(re.search(r'[%s]' % string.punctuation, response.json()))
        assert not bool(re.search(r'\d', response.json()))
        assert not bool(re.search(r'[a-z]', response.json()))


def test_create_password_only_digits(client: TestClient):
    data = {
        "length": 10,
        "digits": True
    }
    response = client.post(f"{settings.API_STR}/create_password/", json=data)
    status_code, detail = check_data(data)
    assert response.status_code == status_code
    if response.status_code != status_code:
        assert response.json()["detail"] == detail
    else:
        assert not bool(re.search(r'[%s]' % string.punctuation, response.json()))
        assert not bool(re.search(r'[a-z]', response.json()))
        assert not bool(re.search(r'[A-Z]', response.json()))


def test_create_password_only_symbols(client: TestClient):
    data = {
        "length": 10,
        "symbols": True
    }
    response = client.post(f"{settings.API_STR}/create_password/", json=data)
    status_code, detail = check_data(data)
    assert response.status_code == status_code
    if response.status_code != status_code:
        assert response.json()["detail"] == detail
    else:
        assert not bool(re.search(r'\d', response.json()))
        assert not bool(re.search(r'[a-z]', response.json()))
        assert not bool(re.search(r'[A-Z]', response.json()))
