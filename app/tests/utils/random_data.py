import random
from typing import Tuple


def generate_random_data() -> dict:
    return {
        "length": random.randint(0, 500),
        "digits": random.choice([True, False]),
        "lowercase": random.choice([True, False]),
        "uppercase": random.choice([True, False]),
        "symbols": random.choice([True, False])
    }


def check_data(data: dict) -> Tuple[int, str]:
    if data.get("length"):
        if data.get("length") > 200:
            return 400, "Password can't be longer than 200 characters"
        if data.get("length") == 0:
            return 400, "Password length can't be equal to 0"
    if data.get("length") is None:
        return 400, "Password length has to be set"
    if all([not data.get("lowercase"), not data.get("uppercase"), not data.get("digits"), not data.get("symbols")]):
        return 400, "Password can't be generated with no valid character"
    else:
        return 200, "Password can be generated"
