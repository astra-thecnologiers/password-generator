from fastapi import FastAPI
from fastapi.responses import HTMLResponse

from app.api.api import api_router
from app.core import settings

tags = [
    {
        "name": "info",
        "description": "You can get information about server configuration.",
    },
    {
        "name": "create_password",
        "description": "Choose your configuration and let the magic happen.",
    },
]

app = FastAPI(openapi_tags=tags)
settings = settings.Settings()


@app.get("/", response_class=HTMLResponse)
async def root():
    return """
        <html>
            <head>
                <title>Password Generator</title>
            </head>
            <body>
                <h1>Welcome to Password Generator</h1>
                <h2>If you need to know some help go to <a href="/docs">API Documentation</a></h2>
            </body>
        </html>
        """


app.include_router(api_router, prefix=settings.API_STR)
