import random
import string
from typing import Union


def generate_password(
        pw_length: Union[int, None] = None,
        use_lowercase: Union[bool, None] = False,
        use_uppercase: Union[bool, None] = False,
        use_digits: Union[bool, None] = False,
        use_symbols: Union[bool, None] = False
):
    """

    :param pw_length: The password desired length
    :param use_lowercase: Flag to allow lowercase character
    :param use_uppercase: Flag to allow uppercase character
    :param use_digits: Flag to allow digits
    :param use_symbols: Flag to allow symbols
    :return: The computed random password
    """
    chosen_characters = []

    if use_lowercase:
        chosen_characters += list(string.ascii_lowercase)
    if use_uppercase:
        chosen_characters += list(string.ascii_uppercase)
    if use_digits:
        chosen_characters += list(string.digits)
    if use_symbols:
        chosen_characters += list(string.punctuation)

    pwd = "".join([random.choice(chosen_characters) for _ in range(pw_length)])

    return pwd
