import os

from pydantic import BaseSettings


class Settings(BaseSettings):
    API_STR: str = "/api"
    app_name: str = "Password Generator"
    app_description: str = "This application aim is to create a random password generator"
    pw_length: int = os.getenv("PW_GEN_LENGTH")
    lowercase: bool = os.getenv("PW_GEN_LOWER")
    uppercase: bool = os.getenv("PW_GEN_UPPER")
    digits: bool = os.getenv("PW_GEN_DIGITS")
    symbols: bool = os.getenv("PW_GEN_SYMBOLS")
