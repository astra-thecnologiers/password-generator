from fastapi import APIRouter, HTTPException

from app.core import settings
from app.core.password_generator import generate_password
from app.models.models import PasswordModel

router = APIRouter()
settings = settings.Settings()


@router.post("/", tags=["create_password"])
async def create_password(pw_config: PasswordModel):
    pw_length = pw_config.length if (pw_config.length and pw_config.length > 0) else settings.pw_length
    lowercase = pw_config.lowercase if pw_config.lowercase else settings.lowercase
    uppercase = pw_config.uppercase if pw_config.uppercase else settings.uppercase
    digits = pw_config.digits if pw_config.digits else settings.digits
    symbols = pw_config.symbols if pw_config.symbols else settings.symbols
    if (settings.pw_length and settings.pw_length > 200) or (pw_config.length and pw_config.length > 200):
        raise HTTPException(status_code=400, detail="Password can't be longer than 200 characters")
    if pw_length == 0:
        raise HTTPException(status_code=400, detail="Password length can't be equal to 0")
    if pw_length is None:
        raise HTTPException(status_code=400, detail="Password length has to be set")
    if all([not lowercase, not uppercase, not digits, not symbols]):
        raise HTTPException(status_code=400, detail="Password can't be generated with no valid character")
    pw = generate_password(
            pw_length=pw_length,
            use_lowercase=lowercase,
            use_uppercase=uppercase,
            use_digits=digits,
            use_symbols=symbols,
    )
    return pw
