from fastapi import APIRouter

from app.core import settings

router = APIRouter()
settings = settings.Settings()


@router.get("/", tags=["info"])
async def info():
    return {
        "app_name": settings.app_name,
        "app_description": settings.app_description,
        "pw_length": settings.pw_length,
        "flags": {
            "use_lowercase": settings.lowercase,
            "use_uppercase": settings.uppercase,
            "use_digits": settings.digits,
            "use_symbols": settings.symbols
        }
    }
