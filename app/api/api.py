from fastapi import APIRouter

from app.api.endpoints import info, create_password

api_router = APIRouter()
api_router.include_router(info.router, prefix="/info", tags=["utils"])
api_router.include_router(create_password.router, prefix="/create_password", tags=["create_password"])
