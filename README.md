# PasswordGenerator

This software aim is to let users decide parameters and return them a randomly generated password


## Install and Run

Clone this repository, place yourself into the root directory and follow these steps:

<pre><code>pip install -r requirements</code></pre>

<pre><code>uvicorn app.main:app</code></pre>

You'll be able to send requests to the service at this url: http://127.0.0.1:8000 

## Endpoints
- **/** : this will bring you to a welcome page
- **/docs** : this will bring you to the documentation page
- **/api/info/** : this will show you server configuration 
- **/api/create_password/** : this will let you send password generation parameters

## Server settings
Length and all flags used for password generation can be set server-side.
Just set these environment variables to the desired value:
- **PW_GEN_LENGTH**: length
- **PW_GEN_LOWER**: lowercase 
- **PW_GEN_UPPER**: uppercase
- **PW_GEN_DIGITS**: digits 
- **PW_GEN_SYMBOLS**: symbols

## Docker setting
There are a Dockerfile and a docker-compose.yml file you can use to containerize the application

## Design and thoughts
I started creating a little app with FastAPI without following the best practices just because I thought it were too 
small to create an articulated project structure. Then I started thinking about the tests, the utilities and the
code readability so I decided to follow the guidelines of a big FastAPI project.

I chose to implement also an additional endpoint that could make easier to users know what are the settings of the server due to the
will of changing those settings serverside. 

The structure is:
<pre><code>
password-generator
├── Dockerfile
├── README.md
├── app
│ ├── __init__.py
│ ├── api
│ │ ├── __init__.py
│ │ ├── api.py
│ │ └── endpoints
│ │     ├── __init__.py
│ │     ├── create_password.py
│ │     └── info.py
│ ├── core
│ │ ├── __init__.py
│ │ ├── password_generator.py
│ │ └── settings.py
│ ├── main.py
│ ├── models
│ │ ├── __init__.py
│ │ └── models.py
│ └── tests
│     ├── __init__.py
│     ├── api
│     │ ├── __init__.py
│     │ ├── test_create_password.py
│     │ ├── test_info.py
│     │ └── test_main.py
│     ├── conftest.py
│     └── utils
│         ├── __init__.py
│         └── random_data.py
├── docker-compose.yml
└── requirements.txt
</code></pre>

I divided the modules in a way they could be easily findable and understandable.
I chose to make endpoints async in order to let further massive implementations.

When I created test I was quite confused because of the simpleness of each test I could imagine, so i created a test that
create a random configuration each time it's called to have an aleatory test.

### Thanks for your attention, I hope you'll like this repo.